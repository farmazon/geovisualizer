#ifndef CONNECTPOINTS_H
#define CONNECTPOINTS_H

#include "algorithm.h"

class ConnectPoints : public Algorithm
{
    Q_OBJECT
public:
    explicit ConnectPoints(QObject *parent = 0);

    void start() {}
    bool nextStep();
};

#endif // CONNECTPOINTS_H
