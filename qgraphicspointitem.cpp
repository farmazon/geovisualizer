#include "qgraphicspointitem.h"

#include <QBrush>
#include <QPen>

qreal QGraphicsPointItem::next_width = 1.0;

QGraphicsPointItem::QGraphicsPointItem(QGraphicsItem *parent) :
    QGraphicsEllipseItem(parent), width(next_width)
{
    setBrush(Qt::SolidPattern);
    setPen(QPen(Qt::NoPen));
    update();
}

QGraphicsPointItem::QGraphicsPointItem(qreal x, qreal y, QGraphicsItem *parent)
    : QGraphicsEllipseItem(x - (next_width/2.0), y - (next_width/2.0), next_width, next_width, parent), position(x, y), width(next_width)
{
    setBrush(Qt::SolidPattern);
    setPen(QPen(Qt::NoPen));
    update();
}

void QGraphicsPointItem::setPos(const QPointF &pos)
{
    this->position = pos;
    update_position();
}

void QGraphicsPointItem::setSize(qreal width)
{
    this->width = width;
    next_width = width;
    update_position();
}

void QGraphicsPointItem::update_position()
{
    QGraphicsEllipseItem::setRect(position.x() - width/2.0, position.y() - width/2.0, width, width);
}



