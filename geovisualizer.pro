#-------------------------------------------------
#
# Project created by QtCreator 2013-10-07T13:34:55
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = geovisualizer
TEMPLATE = app
CONFIG += debug

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    qinteractivegraphicsview.cpp \
    algorithm.cpp \
    connectpoints.cpp \
    leftright.cpp \
    predicates.c \
    randompoints.cpp \
    qgraphicspointitem.cpp \
    grahamm.cpp \
    jarvis.cpp \
    graphicobject.cpp \
    segments.cpp \
    drawpolygon.cpp \
    monotonic.cpp \
    triangulacja.cpp

HEADERS  += mainwindow.h \
    qinteractivegraphicsview.h \
    algorithm.h \
    connectpoints.h \
    leftright.h \
    predicates.h \
    randompoints.h \
    qgraphicspointitem.h \
    grahamm.h \
    jarvis.h \
    graphicobject.h \
    segments.h \
    drawpolygon.h \
    monotonic.h \
    triangulacja.h

FORMS    += mainwindow.ui
