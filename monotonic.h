#ifndef MONOTONIC_H
#define MONOTONIC_H

#include "algorithm.h"

class Monotonic : public Algorithm
{
public:
    Monotonic(QObject *parent = 0)
        : Algorithm("Check monotonic", parent) {}

    void start();

private:
    std::pair<Point *, Point *> getNeighbours(int point_id);
    bool second_above(Point * a, Point *b);
};

#endif // MONOTONIC_H
