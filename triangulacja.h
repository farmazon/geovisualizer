#ifndef TRIANGULACJA_H
#define TRIANGULACJA_H

#include "algorithm.h"

#include <QStack>

class Triangulacja : public Algorithm
{
public:
    Triangulacja(QObject *parent = 0)
        : Algorithm("Triangulacja", parent){}
    void start();
    bool nextStep();
private:
    QStack<Point *> stack;
    int next_point;

    void colorLeft(int top_point);
    void colorRight(int top_point);
    bool isLeft(Point *p) { return p->brush().color().red() > 200; }
    bool isRight(Point *p) { return p->brush().color().green() > 200; }
    void pushPoint(Point *p);
    Point *popPoint();
};

#endif // TRIANGULACJA_H
