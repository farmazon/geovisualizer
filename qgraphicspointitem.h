#ifndef QGRAPHICSPOINTITEM_H
#define QGRAPHICSPOINTITEM_H

#include <QGraphicsEllipseItem>

class QGraphicsPointItem : public QGraphicsEllipseItem
{
public:
    explicit QGraphicsPointItem(QGraphicsItem *parent = 0);
    QGraphicsPointItem(qreal x, qreal y, QGraphicsItem *parent=0);

    void setPos(const QPointF &pos);
    void setSize(qreal width);
    QPointF pos() const { return position; }
    qreal x() const { return pos().x(); }
    qreal y() const { return pos().y(); }
    qreal size() const { return width; }

    static void setMainSize(qreal width) { next_width = width; }
private:
    QPointF position;
    qreal width;

    static qreal next_width;

    void update_position();
};

#endif // QGRAPHICSPOINTITEM_H
