#ifndef RANDOM100POINTS_H
#define RANDOM100POINTS_H

#include "algorithm.h"

double randomdbl(double min, double max);

class RandomNPoints : public Algorithm
{
    Q_OBJECT
public:
    explicit RandomNPoints(QObject *parent = 0);
    void start();
};

class RandomOnLine : public Algorithm
{
    Q_OBJECT
public:
    explicit RandomOnLine(QObject *parent = 0);
    void start();
};

class RandomPointsOnCircle : public Algorithm
{
    Q_OBJECT
public:
    explicit RandomPointsOnCircle(QObject *parent = 0);
    void start();
};

class RandomPointsOnSquare : public Algorithm
{
    Q_OBJECT
public:
    explicit RandomPointsOnSquare(QObject *parent = 0)
        : Algorithm("RandomPointsOnSquare", parent) {}
    void start();
};

#endif // RANDOM100POINTS_H
