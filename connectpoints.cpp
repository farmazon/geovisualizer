#include "connectpoints.h"

#include <QMessageBox>

ConnectPoints::ConnectPoints(QObject *parent) :
    Algorithm(parent)
{
    setObjectName("ConnectPoints");
}

bool ConnectPoints::nextStep()
{
    if (points.size() >= 2) {
        QPointF point1 = points.back()->pos();
        points.pop_back();
        QPointF point2 = points.back()->pos();
        points.pop_back();
        emit drawLine(point1, point2);
        return false;
    } else {
        QMessageBox::information(NULL, "Connect points", "Algorithm finished", QMessageBox::Ok);
        return true;
    }
}
