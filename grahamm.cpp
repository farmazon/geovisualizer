#include "grahamm.h"

#include <algorithm>
#include <QMessageBox>


namespace {

double abs(double x) {
    return x < 0.0 ? -x : x;
}

double det1(const QPointF &a, const QPointF &b, const QPointF &c) {
    return (b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x());
}

QPointF main;

bool comparePoints(QGraphicsPointItem *a, QGraphicsPointItem *b) {
    if (a->pos() == main) {
        return true;
    } else if (b->pos() == main) {
        return false;
    }
    return det1(main, a->pos(), b->pos()) > 0;
}

}

void Grahamm::start()
{
    if (points.size() < 3) {
        QMessageBox::warning(0, "oops!", "More Points!", QMessageBox::Ok);
        next_point = -1;
        return;
    }

    Point *first = points.first();
    foreach(Point *point, points) {
        if (point->y() < first->y() || (point->y() == first->y() && point->x() < first->x())) {
            first = point;
        }
    }
    main = first->pos();

    qSort(points.begin(), points.end(), comparePoints);
    if (points[0] != first) {
        abort();
    }
    push_line(points[0]->pos(), points[1]->pos());
    result.push(points[0]);
    result.push(points[1]);
    next_point = 2;
}

bool Grahamm::nextStep()
{
    if (next_point >= points.size()) {
        push_line(stack.top()->line().p2(), main);
        colorize_points();
        next_point = -1;
    }
    if (next_point < 0) {
        return false;
    }

    QPointF a = stack.top()->line().p1();
    QPointF b = stack.top()->line().p2();
    QPointF c = points[next_point]->pos();
    double res = det1(a,b,c);
    if (res > 0.0) {
        push_line(b, c);
        result.push(points[next_point]);
        ++next_point;
    } else if(res < 0.0) {
        emit clearItem(stack.top());
        delete stack.top();
        stack.pop();
        result.pop();
    } else {
        if (abs(c.x() -  a.x()) + abs(c.y() - a.y()) > abs(b.y() - a.y()) + abs(b.x() - a.x())) {
            emit clearItem(stack.top());
            delete stack.top();
            stack.pop();
            result.pop();

            push_line(a, c);
            result.push(points[next_point]);
        }
        ++next_point;
    }

    return true;
}

void Grahamm::push_line(const QPointF &p1, const QPointF &p2)
{
    Line *line = new Line(QLineF(p1, p2));
    emit drawItem(line);
    stack.push(line);
}

void Grahamm::colorize_points()
{
    while(!result.empty()) {
        //QPointF pos = result.top()->pos();
        result.top()->setBrush(QBrush(QColor(0, 212,0)));
        emit clearItem(result.top());
        emit drawItem(result.top());
        result.pop();
    }
}
