#include "monotonic.h"

#include <QMessageBox>
#include <utility>

#include "predicates.h"

using namespace std;

void Monotonic::start()
{
    bool monotonic = true;
    for(int i = 0; i < points.size(); ++i) {
        pair<Point *, Point *> neigh = getNeighbours(i);
        if (second_above(neigh.first, points[i]) && second_above(neigh.second, points[i])) {
            if (det1(points[i]->pos(), neigh.first->pos(), neigh.second->pos()) > 0.0) {
                points[i]->setColor(0, 255, 0);
            } else {
                points[i]->setColor(150, 150, 255);
                monotonic = false;
            }
        } else if (second_above(points[i], neigh.first) && second_above(points[i], neigh.second)) {
            if (det1(points[i]->pos(), neigh.first->pos(), neigh.second->pos()) > 0.0) {
                points[i]->setColor(255, 0, 0);
            } else {
                points[i]->setColor(0, 0, 255);
                monotonic = false;
            }
        } else {
            points[i]->setColor(0x90, 0x60, 0);
        }
    }
    if (monotonic) {
        QMessageBox::information(0, "Result", "Wielokąt jest monotoniczny.", QMessageBox::Ok);
    } else {
        QMessageBox::information(0, "Result", "Wielokąt nie jest monotoniczny.", QMessageBox::Ok);
    }
}

std::pair<Point *, Point *> Monotonic::getNeighbours(int point_id)
{
    if (point_id > 0 && point_id < points.size()-1) {
        return make_pair(points[point_id-1], points[point_id+1]);
    } else if (point_id == 0) {
        return make_pair(points.last(), points[1]);
    } else if (point_id == points.size()-1){
        return make_pair(points[point_id-1], points.first());
    } else {
        throw 14;
    }
}

bool Monotonic::second_above(Point *a, Point *b)
{
    return a->y() == b->y() ? b->x() > a->x() : b->y() > a->y();
}
