#include "qinteractivegraphicsview.h"

#include <QMouseEvent>
#include <fstream>
#include <limits>

#include "graphicobject.h"

using namespace std;

QInteractiveGraphicsView::QInteractiveGraphicsView(QWidget *parent) :
    QGraphicsView(parent),
    next_item(SS_NONE),
    zoom(4.0)
{
    setScene(new QGraphicsScene(this));
    scene()->setSceneRect(-100.0, -100.0, 100.0, 100.0);
    setZoom(zoom);
}

void QInteractiveGraphicsView::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);
    clicked = true;
    switch(next_item) {
    case SS_POINT:
        drawPoint(mapToScene(event->pos()));
        break;
    case SS_LINE:
        first_click = mapToScene(event->pos());
        break;
    case SS_CLEAR:
        clearItem(mapToScene(event->pos()));
        break;
    default:
        break;
    }
}

void QInteractiveGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    QGraphicsView::mouseReleaseEvent(event);
    switch(next_item) {
    case SS_LINE:
        drawLine(first_click, mapToScene(event->pos()));
        break;
    default:
        break;
    }
    clicked = false;
}

void QInteractiveGraphicsView::loadRecord(ifstream &in)
{
    SupportedShape shape;
    in >> (int &)shape;
    switch (shape) {
    case SS_POINT:
        double x, y;
        in >> x >> y;
        drawPoint(QPointF(x, y));
        break;
    case SS_LINE:
        double x1, y1, x2, y2;
        in >> x1 >> y1 >> x2 >> y2;
        drawLine(QPointF(x1, y1), QPointF(x2, y2));
        break;
    default:
        break;
    }
}

void QInteractiveGraphicsView::saveRecord(ofstream &out, QGraphicsItem *record)
{
    Point *point = dynamic_cast<Point *>(record);
    if (point != NULL) {
        out << (int) SS_POINT << " " << fixed << point->x() << " " << fixed << point->y() << endl;
        return;
    }
    Line *line = dynamic_cast<Line *>(record);
    if (line != NULL) {
        out << (int) SS_LINE << " " << fixed << line->line().x1() << " " << fixed << line->line().y1() << " "
            << fixed << line->line().x2() << " " << fixed << line->line().y2() << endl;
        return;
    }
    throw 14;
}

void QInteractiveGraphicsView::drawItem(QGraphicsItem *item)
{
    scene()->addItem(item);
}

void QInteractiveGraphicsView::drawPoint(const QPointF &coord)
{
    Point *new_point = new Point(coord.x(), coord.y());
    scene()->addItem(new_point);
}

void QInteractiveGraphicsView::clearItem(const QPointF &at)
{
    QGraphicsItem *removed = itemAt(mapFromScene( at));
    if (removed != NULL) {
        scene()->removeItem(removed);
        delete removed;
    }
}

void QInteractiveGraphicsView::drawLine(const QPointF &begin, const QPointF &end)
{
    Line *new_line = new Line(begin.x(), begin.y(), end.x(), end.y());
    scene()->addItem(new_line);
}

void QInteractiveGraphicsView::clearItem(QGraphicsItem *item)
{
    scene()->removeItem(item);
}

void QInteractiveGraphicsView::clearAll()
{
    scene()->clear();
}

void QInteractiveGraphicsView::loadFrom(const QString &filename)
{
    ifstream in;
    in.exceptions(ifstream::failbit | ifstream::badbit | ifstream::eofbit);
    try {
        in.open(filename.toStdString().c_str());
        while(!in.eof()) {
            loadRecord(in);
        }
    } catch(...) {

    }
}

void QInteractiveGraphicsView::saveTo(const QString &filename)
{
    ofstream out;
    out.exceptions(ofstream::failbit | ofstream::badbit);
    out.precision(numeric_limits<double>::digits10);
    try {
        out.open(filename.toStdString().c_str());
        foreach(QGraphicsItem *it, scene()->items()) {
            saveRecord(out, it);
        }
    } catch(...) {

    }
}

void QInteractiveGraphicsView::setZoom(double zoom)
{
    this->zoom = zoom;
    setTransform(QTransform());
    scale(zoom, -zoom);

    QGraphicsPointItem::setMainSize(1.0);
    for(QGraphicsItem *item : scene()->items()) {
        Point *point = dynamic_cast<Point *>(item);
        if (point != NULL) {
            point->setSize(1.0);
        }
    }
}

