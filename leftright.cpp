#include "leftright.h"

#include <QBrush>
#include <QPen>
#include <QMessageBox>
#include <sstream>
#include <ctime>

#include "predicates.h"

using namespace std;

const QPointF LeftRight::a = QPointF(-1.0, 0.0);
const QPointF LeftRight::b = QPointF(1.0, 0.1);

double LeftRight::test(const QPointF &a, const QPointF &b, const QPointF &c) {
    return (b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x());
}


void LeftRight::start()
{
    int green = 0, red = 0, blue = 0;

    double results[points.size()];

    int i = 0;
    clock_t start, stop;
    start = clock();
    foreach(QGraphicsPointItem *point, points) {
        results[i] = test(a,b,point->pos());
        ++i;
    }
    stop = clock();

    i = 0;
    foreach(QGraphicsPointItem *point, points) {
        if (results[i] > 0.0) {
            point->setBrush(QBrush(QColor(255, 0, 0), Qt::SolidPattern));
            ++red;
        } else if(results[i] < 0.0) {
            point->setBrush(QBrush(QColor(0, 255, 0), Qt::SolidPattern));
            ++green;
        } else {
            point->setBrush(QBrush(QColor(0, 0, 255), Qt::SolidPattern));
            ++blue;
        }
        ++i;
    }

    stringstream result;
    result << "Rezultat: punkty czerwone: " << red << ", zielone: " << green << ", niebieskie: " << blue << endl;
    result << "Czas działania: " << (double)(stop - start) / (double)(CLOCKS_PER_SEC);
    QMessageBox::information(0, "", result.str().c_str(), QMessageBox::Ok);
}

double LeftRightExact::test(const QPointF &a, const QPointF &b, const QPointF &c)
{
    double aa[2];
    double bb[2];
    double cc[2];
    aa[0] = a.x();
    aa[1] = a.y();
    bb[0] = b.x();
    bb[1] = b.y();
    cc[0] = c.x();
    cc[1] = c.y();
    return orient2dexact(aa, bb, cc);
}

double LeftRightFast::test(QPointF a, QPointF b, QPointF c)
{
    double aa[2];
    double bb[2];
    double cc[2];
    aa[0] = a.x();
    aa[1] = a.y();
    bb[0] = b.x();
    bb[1] = b.y();
    cc[0] = c.x();
    cc[1] = c.y();
    return orient2dfast(aa, bb, cc);
}


double LeftRightSlow::test(const QPointF &a, const QPointF &b, const QPointF &c)
{
    double aa[2];
    double bb[2];
    double cc[2];
    aa[0] = a.x();
    aa[1] = a.y();
    bb[0] = b.x();
    bb[1] = b.y();
    cc[0] = c.x();
    cc[1] = c.y();
    return orient2dslow(aa, bb, cc);
}


double LeftRightAnotherDet::test(const QPointF &a, const QPointF &b, const QPointF &c)
{
    return a.x()*b.y() + a.y()*c.x() + b.x()*c.y() - c.x()*b.y() - a.x()*c.y() - b.x()*a.y();
}
