#include "randompoints.h"

#include <ctime>
#include <cstdlib>
#include <cmath>
#include <QInputDialog>

RandomNPoints::RandomNPoints(QObject *parent) :
    Algorithm(parent)
{
    setObjectName("RandomNPoints");
}

void RandomNPoints::start()
{
    //Algorithm::start(list);
    srand(time(NULL));
    int n = QInputDialog::getInt(0, "How many points?", "", 100000, 0);
    double min = QInputDialog::getDouble(0, "Minimum value", "", -100.0, -10e14, 0.0);
    double max = QInputDialog::getDouble(0, "Maximum value", "", 100.0, 0.0, 10e14);
    for(int i = 0; i < n; ++i) {
        emit drawPoint(QPointF(randomdbl(min, max), randomdbl(min, max)));
    }
}

double randomdbl(double min, double max)
{
    return min + rand()*(max-min) / (double)(RAND_MAX);
}

RandomOnLine::RandomOnLine(QObject *parent) :
    Algorithm(parent)
{
    setObjectName("RandomOnLine");
}

void RandomOnLine::start()
{
    int n = QInputDialog::getInt(0, "", "How many?", 100000, 0);
    double min = QInputDialog::getDouble(0, "", "Minimum x:", -1000.0, -10e14, 14e14, 3);
    double max = QInputDialog::getDouble(0, "", "Minimum x:", 1000.0, -10e14, 14e14, 3);

    for(int i = 0; i < n; ++i) {
        double x = randomdbl(min, max);
        double y = 0.05*x + 0.05;
        emit drawPoint(QPointF(x, y));
    }
}

RandomPointsOnCircle::RandomPointsOnCircle(QObject *parent) :
    Algorithm(parent)
{
    setObjectName("RandomPointsOnCircle");
}

void RandomPointsOnCircle::start()
{
    int n = QInputDialog::getInt(0, "How many?", "", 100000, 0, 100000);
    double r = QInputDialog::getDouble(0, "Radius:", "", 1.0, 0.0, 10e14, 3);

    srand(time(NULL));
    for(int i = 0; i < n; ++i) {
        double alpha = randomdbl(0.0, 2*M_PI);
        //double a = randomdbl(0.0, r);
        emit drawPoint(QPointF(sin(alpha)*r, cos(alpha)*r));
    }
}


void RandomPointsOnSquare::start()
{
    int n = QInputDialog::getInt(0, "How many on border x,y?", "", 100, 0, 100000);
    int m = QInputDialog::getInt(0, "How many on diagonal?", "", 0, 0, 100000);
    int o = QInputDialog::getInt(0, "How many on borders?", "", 100, 0, 100000);
    double x1 = QInputDialog::getDouble(0, "x1", "", -10.0, -10e14, 10e14, 3);
    double y1 = QInputDialog::getDouble(0, "y1", "", -10.0, -10e14, 10e14, 3);
    double a = QInputDialog::getDouble(0, "Size", "", 20.0, 0.0, 10e14, 3);
    double x2 = x1+a;
    double y2 = y1+a;

    srand(time(NULL));
    for(int i = 0; i < n; ++i) {
        double offset = randomdbl(0.0, a);
        switch(rand()&1) {
        case 0:
            emit drawPoint(QPointF(x1+offset, y1));
            break;
        case 1:
            emit drawPoint(QPointF(x1, y1+offset));
            break;
        }
    }

    for(int i = 0; i < m; ++i) {
        double offset = randomdbl(0.0, a);
        switch(rand()&1) {
        case 0:
            emit drawPoint(QPointF(x1+offset, y1+offset));
            break;
        case 1:
            emit drawPoint(QPointF(x2-offset, y1+offset));
            break;
        }
    }

    for(int i = 0; i < o; ++i) {
        double offset = randomdbl(0.0, a);
        switch(rand()&1) {
        case 0:
            emit drawPoint(QPointF(x1+offset, y2));
            break;
        case 1:
            emit drawPoint(QPointF(x2, y1+offset));
            break;
        }
    }
}
