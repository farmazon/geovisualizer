#ifndef DRAWPOLYGON_H
#define DRAWPOLYGON_H

#include "algorithm.h"

class DrawPolygon : public Algorithm
{
public:
    DrawPolygon(QObject *parent = 0)
        : Algorithm("Draw polygon", parent){}

    void start();
};

#endif // DRAWPOLYGON_H
