#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QGLWidget>

#include "qinteractivegraphicsview.h"
#include "connectpoints.h"
#include "leftright.h"
#include "randompoints.h"
#include "grahamm.h"
#include "jarvis.h"
#include "segments.h"
#include "drawpolygon.h"
#include "monotonic.h"
#include "triangulacja.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    current(NULL),
    algorithmMapper(new QSignalMapper(this))
{
    ui->setupUi(this);
    view =  ui->centralWidget->findChild<QInteractiveGraphicsView *>();
    view->setNextElement(SS_NONE);
    view->setSceneRect(-100.0, -100.0, 200.0, 200.0);
    view->scale(3.0, 3.0);
    view->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering)));
    view->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

    QObject::connect(algorithmMapper, SIGNAL(mapped(QString)), this, SLOT(runAlgorithm(QString)));
    algorithms_menu = ui->menuBar->findChild<QMenu *>("menuAlgorithms");

    createAlgorithm<ConnectPoints>();
    createAlgorithm<RandomNPoints>();
    createAlgorithm<RandomPointsOnCircle>();
    createAlgorithm<RandomOnLine>();
    createAlgorithm<RandomPointsOnSquare>();
    /*createAlgorithm<Grahamm>();
    createAlgorithm<Jarvis>();
    createAlgorithm<Segments>();*/
    createAlgorithm<DrawPolygon>();
    createAlgorithm<Monotonic>();
    createAlgorithm<Triangulacja>();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::save()
{
    QString name = QFileDialog::getSaveFileName(this);
    emit saveTo(name);
}

void MainWindow::load()
{
    QString name = QFileDialog::getOpenFileName(this);
    emit loadFrom(name);
}

void MainWindow::runAlgorithm(QString name)
{
    for(QList<Algorithm *>::iterator it = algorithms.begin(); it != algorithms.end(); it++) {
        if ((*it)->objectName() == name) {
            current = *it;
            current->prepare(view->scene()->items());
            current->start();
        }
    }
}

void MainWindow::resizeArea()
{
    double min = QInputDialog::getDouble(this, "From:", "", -100.0, -10e14, 10e14, 3);
    double max = QInputDialog::getDouble(this, "To:", "", 100.0, -10e14, 10e14, 3);
    //double scale = QInputDialog::getDouble(this, "Scale:", "", 1.0, 0.0, 10000.0, 14);
    view->setSceneRect(min, min, max-min, max-min);

}

void MainWindow::nextStep()
{
    if (current != NULL) {
        current->nextStep() || QMessageBox::information(this, "", "Algorithm finished");
    }
}

void MainWindow::finish()
{
    if (current != NULL) {
        while(current->nextStep());
    }
}

template<class A>
void MainWindow::createAlgorithm()
{
    A *new_alg = new A(this);
    QObject::connect(new_alg, SIGNAL(drawItem(QGraphicsItem*)), view, SLOT(drawItem(QGraphicsItem*)));
    QObject::connect(new_alg, SIGNAL(drawPoint(QPointF)), view, SLOT(drawPoint(QPointF)));
    QObject::connect(new_alg, SIGNAL(drawLine(QPointF,QPointF)), view, SLOT(drawLine(QPointF,QPointF)));
    QObject::connect(new_alg, SIGNAL(clearItemAt(QPointF)), view, SLOT(clearItem(QPointF)));
    QObject::connect(new_alg, SIGNAL(clearItem(QGraphicsItem*)), view, SLOT(clearItem(QGraphicsItem*)));
    QObject::connect(new_alg, SIGNAL(clearAll()), view, SLOT(clearAll()));

    QAction *new_action = algorithms_menu->addAction(new_alg->objectName());
    algorithmMapper->setMapping(new_action, new_alg->objectName());
    QObject::connect(new_action, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithms.push_back(new_alg);
}
