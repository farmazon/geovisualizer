#ifndef QINTERACTIVEGRAPHICSVIEW_H
#define QINTERACTIVEGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsItem>

enum SupportedShape {
    SS_NONE = -1,
    SS_CLEAR = -2,
    SS_BEGIN = 0,
    SS_POINT = 0,
    SS_LINE,
    SS_END
};

class QInteractiveGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit QInteractiveGraphicsView(QWidget *parent = 0);
    SupportedShape next_item;
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
private:
    bool clicked;
    QPointF first_click;

    double zoom;

    void loadRecord(std::ifstream &in);
    void saveRecord(std::ofstream &out, QGraphicsItem *record);
signals:

public slots:
    void setNextElement(SupportedShape shape) { next_item = shape; }
    void setNextPoint() { next_item = SS_POINT; }
    void setNextClear() { next_item = SS_CLEAR; }
    void setNextNone() { next_item = SS_NONE; }
    void setNextLine() { next_item = SS_LINE; }
    void drawItem(QGraphicsItem *item);
    void drawPoint(const QPointF &coord);
    void clearItem(const QPointF &at);
    void drawLine(const QPointF &begin, const QPointF &end);
    void clearItem(QGraphicsItem *item);
    void clearAll();

    void loadFrom(const QString &filename);
    void saveTo(const QString &filename);

    void setZoom(double zoom);
    void zoomIn() { setZoom(zoom*2.0); }
    void zoomOut() { setZoom(zoom/2.0); }
};

#endif // QINTERACTIVEGRAPHICSVIEW_H
