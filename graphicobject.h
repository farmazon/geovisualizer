#ifndef GRAPHICOBJECT_H
#define GRAPHICOBJECT_H

#include <QGraphicsItem>
#include <QGraphicsLineItem>
#include <QPen>
#include <QBrush>
#include <qgraphicspointitem.h>

template<class GraphicsItem>
class GraphicObject : public GraphicsItem
{
public:
    template<typename ...Args>
    GraphicObject(Args... args)
        : GraphicsItem(args...) {
        GraphicsItem::setPen(QPen(QBrush(Qt::SolidPattern), 0.25));
    }

    void setColor(int r, int g, int b) {
        GraphicsItem::setBrush(QBrush(QColor(r, g, b), Qt::SolidPattern));
    }
};

typedef GraphicObject<QGraphicsPointItem> Point;
typedef GraphicObject<QGraphicsLineItem> Line;

#endif // GRAPHICOBJECT_H
