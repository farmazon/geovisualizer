#include "triangulacja.h"
#include "predicates.h"

bool compareY(Point *a, Point *b) {
    return a->y() == b->y() ? a->x() > b->x() : a->y() > b->y();
}

void Triangulacja::start()
{
    int top_point = 0;
    for(int i = 1; i < points.size(); ++i) {
        if (compareY(points[i], points[top_point])) {
            top_point = i;
        }
    }
    colorLeft(top_point);
    colorRight(top_point);

    qSort(points.begin(),points.end(), compareY);

    stack.clear();
    pushPoint(points[0]);
    pushPoint(points[1]);
    next_point = 2;
}

bool Triangulacja::nextStep()
{
    if (next_point >= points.size()-1) {
        return false;
    }
    Point *point = points[next_point];

    if (isLeft(point) == isRight(stack.top())) {
        Point *last_point = stack.top();
        while (stack.size() > 1) {
            emit drawLine(point->pos(), popPoint()->pos());
        }
        popPoint();
        pushPoint(last_point);
        pushPoint(point);
    } else {
        Point *last_point = popPoint();
        while (!stack.empty()) {
             double side = det1(point->pos(), last_point->pos(), stack.top()->pos());
            if ((isLeft(point) && side > 0.0) ||
                    (isRight(point) && side < 0.0)) {
                emit drawLine(point->pos(), stack.top()->pos());
                last_point = popPoint();
            } else {
                break;
            }
        }
        pushPoint(last_point);
        pushPoint(point);
    }
    ++next_point;

    return true;
}

void Triangulacja::colorRight(int top_point)
{
    int i = top_point;
    Point *last_point;
    do {
        points[i]->setColor(0, 255, 0);
        last_point = points[i];
        --i;
        if (i < 0) i = points.size()-1;
    } while (compareY(last_point, points[i]));
}

void Triangulacja::colorLeft(int top_point)
{
    int i = top_point;
    Point *last_point;
    do {
        points[i]->setColor(255, 0, 0);
        last_point = points[i];
        ++i;
        if (i >= points.size()) i = 0;
    } while (compareY(last_point, points[i]));
}

void Triangulacja::pushPoint(Point *p)
{
    p->setColor(p->brush().color().red(), p->brush().color().green(), 255);
    stack.push(p);
}

Point *Triangulacja::popPoint()
{
    Point *p = stack.pop();
    p->setColor(p->brush().color().red(), p->brush().color().green(), 0);
    return p;
}
