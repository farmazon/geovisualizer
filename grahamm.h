#ifndef GRAHAMM_H
#define GRAHAMM_H

#include "algorithm.h"
#include "stack"

class Grahamm : public Algorithm
{
    Q_OBJECT
public:
    explicit Grahamm(QObject *parent = 0)
        : Algorithm("Grahamm", parent), next_point(-1) {}

    void start();
    bool nextStep();
private:
    std::stack<Point *> result;
    std::stack<Line *> stack;
    int next_point;

    void push_line(const QPointF &p1, const QPointF &p2);
    void colorize_points();
};

#endif // GRAHAMM_H
