#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <QObject>
#include "graphicobject.h"

class Algorithm : public QObject
{
    Q_OBJECT
public:
    explicit Algorithm(QObject *parent = 0);
    Algorithm(const QString &name, QObject *parent=0);

    virtual void prepare(const QList<QGraphicsItem *> &list);
    virtual void start()=0;
    virtual bool nextStep() { return false; }
protected:
    QList<Point *> points;
    QList<Line *> lines;
signals:
    void drawItem(QGraphicsItem *item);
    void drawPoint(const QPointF &coord);
    void clearItemAt(const QPointF &at);
    void clearItem(QGraphicsItem *item);
    void clearAll();
    void drawLine(const QPointF &begin, const QPointF &end);

public slots:

};

#endif // ALGORITHM_H
