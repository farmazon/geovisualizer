#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSignalMapper>

#include "algorithm.h"
#include "qinteractivegraphicsview.h"
#include "randompoints.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QList<Algorithm *> algorithms;
    Algorithm *current;

    QInteractiveGraphicsView *view;
    QSignalMapper *algorithmMapper;
    QMenu *algorithms_menu;

    template<class A>
    void createAlgorithm();
public slots:
    void save();
    void load();
    void runAlgorithm(QString name);
    void resizeArea();
    void nextStep();
    void finish();
signals:
    void saveTo(const QString &filename);
    void loadFrom(const QString &filename);
};

#endif // MAINWINDOW_H
