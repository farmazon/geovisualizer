#include "algorithm.h"

Algorithm::Algorithm(QObject *parent) :
    QObject(parent)
{
}

Algorithm::Algorithm(const QString &name, QObject *parent)
    : QObject(parent)
{
    setObjectName(name);
}

void Algorithm::prepare(const QList<QGraphicsItem *> &list)
{
    points.clear();
    lines.clear();
    foreach(QGraphicsItem *it, list) {
        Point *point = dynamic_cast<Point *>(it);
        if (point != NULL) {
            points.push_back(point);
            continue;
        }
        Line *line = dynamic_cast<Line *>(it);
        if (line != NULL) {
            lines.push_back(line);
        }
    }
}
