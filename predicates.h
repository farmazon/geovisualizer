#ifndef PREDICATES_H
#define PREDICATES_H

#include <QPointF>
#include <QLineF>

extern "C" {

double orient2dfast(double *pa, double *pb, double *pc);
double orient2dexact(double *pa, double *pb, double *pc);
double orient2dslow(double *pa, double *pb, double *pc);

}

namespace {

double det1(const QPointF &a, const QPointF &b, const QPointF &c) {
    return (b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x());
}

double operator*(const QPointF &a, const QPointF &b) {
    return a.x()*b.y() - a.y()*b.x();
}

double dist(const QPointF &a, const QPointF &b) {
    return (a.x() - b.x())*(a.x()- b.x()) + (a.y() - b.y())*(a.y()-b.y());
}

double point_of_intersection(const QLineF &a, const QLineF &b) {
    QPointF r = a.p2() - a.p1();
    QPointF s = b.p2() - b.p1();
    return (b.p1() - a.p1())*s / (r*s);
}

double cutting_segment(const QLineF &a, const QLineF &b) {
    return det1(a.p1(), a.p2(), b.p1())*det1(a.p1(), a.p2(), b.p2()) <= 0
            && det1(b.p1(), b.p2(), a.p1())*det1(b.p1(), b.p2(), a.p2()) <= 0;
}

}

#endif // PREDICATES_H
