#include "mainwindow.h"
#include <QApplication>
#include <iostream>

int main(int argc, char *argv[])
{
    std::ios_base::sync_with_stdio(0);
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
