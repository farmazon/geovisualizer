#ifndef JARVIS_H
#define JARVIS_H

#include "algorithm.h"
#include <QStack>

class Jarvis : public Algorithm
{
public:
    Jarvis(QObject *parent) : Algorithm("Jarvis", parent) {}
    void start();
    bool nextStep();
private:
    Point *last, *first;
    QStack<Line *> lines_on;
    QStack<Point *> points_on;
};

#endif // JARVIS_H
