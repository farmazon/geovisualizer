#ifndef LEFTRIGHT_H
#define LEFTRIGHT_H

#include "algorithm.h"

class LeftRight : public Algorithm
{
    Q_OBJECT
public:
    explicit LeftRight(QObject *parent) :
        Algorithm("LeftRight", parent) {}
    LeftRight(const QString &name, QObject *parent = 0) :
        Algorithm(name, parent) {}
    static const QPointF a, b;

    virtual double test(const QPointF &a, const QPointF &b, const QPointF &c);
    void start();
};

class LeftRightAnotherDet : public LeftRight
{
    Q_OBJECT
public:
    explicit LeftRightAnotherDet(QObject *parent = 0)
        : LeftRight("LeftRightAnotherDet", parent) {}

    virtual double test(const QPointF &a, const QPointF &b, const QPointF &c);
};

class LeftRightExact : public LeftRight
{
    Q_OBJECT
public:
    explicit LeftRightExact(QObject *parent = 0)
        : LeftRight("LeftRightExact", parent) {}
    double test(const QPointF &a, const QPointF &b, const QPointF &c);
};

class LeftRightFast : public LeftRight
{
    Q_OBJECT
public:
    explicit LeftRightFast(QObject *parent = 0)
        : LeftRight("LeftRightFast", parent) {}

    double test(QPointF a, QPointF b, QPointF c);
};

class LeftRightSlow : public LeftRight
{
    Q_OBJECT
public:
    explicit LeftRightSlow(QObject *parent = 0)
        : LeftRight("LeftRightSlow", parent) {}
    double test(const QPointF &a, const QPointF &b, const QPointF &c);
};

#endif // LEFTRIGHT_H
