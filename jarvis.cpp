#include "jarvis.h"

#include "predicates.h"


void Jarvis::start()
{
    first = points.first();
    foreach(Point *point, points) {
        if (point->y() < first->y() || (point->y() == first->y() && point->x() < first->x())) {
            first = point;
        }
    }
    last = first;
    //last->setBrush(QColor(0, 212, 0));
}

bool Jarvis::nextStep()
{
    if (last == first && last->brush().color().green() > 0) {
        return false;
    }
    Point *next = points.first();
    foreach(Point *point, points) {
        if (point->brush().color().green() > 0) {
            continue;
        }
        double res = det1(last->pos(), next->pos(), point->pos());
        if (res > 0.0) {
            next = point;
        } else if (res == 0.0 && dist(next->pos(), last->pos()) < dist(point->pos(), last->pos())) {
            next = point;
        }
    }
    next->setBrush(QColor(0, 212, 0));
    emit clearItem(next);
    emit drawItem(next);
    emit drawItem(new Line(QLineF(last->pos(), next->pos())));
    last = next;

    return true;
}
