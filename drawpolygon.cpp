#include "drawpolygon.h"

void DrawPolygon::start()
{
    for(int i = 0; i < points.size()-1; ++i) {
        emit drawLine(points[i]->pos(), points[i+1]->pos());
    }
    emit drawLine(points.last()->pos(), points.first()->pos());
}
