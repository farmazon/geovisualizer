#ifndef SEGMENTS_H
#define SEGMENTS_H

#include "algorithm.h"
#include <set>
#include <memory>

struct ComparableLine : public Line {
    QPointF relative_point;

    ComparableLine(const Line &line)
        : Line(line.line()), relative_point(line.line().p1())
    {}
};

struct EndPoint : public Point {
    ComparableLine *line;

    EndPoint(ComparableLine *line, const QPointF &pos)
        : Point(pos.x(), pos.y()), line(line) {}
};

struct PointOfIntersection : Point {
    ComparableLine *line1, *line2;

    PointOfIntersection(const QPointF &point, ComparableLine *line1, ComparableLine *line2)
        : Point(point.x(), point.y()), line1(line1), line2(line2) {}
};

bool operator<(const QPointF &a, const QPointF &b);

struct compareX {
    bool operator()(Point *a, Point *b);
};
struct compareLines {
    bool operator()(ComparableLine *a, ComparableLine *b);
};

class Segments : public Algorithm
{
public:
    Segments(QObject *parent)
        : Algorithm("Segment intersection", parent) {}
    ~Segments();

    void start();
    bool nextStep();
private:
    QList<ComparableLine *> clines;
    std::multiset<Point *, compareX> sorted_points;
    std::set<ComparableLine *, compareLines> current_lines;
    std::set<std::pair<ComparableLine *, ComparableLine *>> compared;

    Line *miotla;

    void linesBegin(EndPoint *point);
    void linesEnd(EndPoint *point);
    void intesectionPoint(PointOfIntersection *point);

    void checkPrev(const std::set<ComparableLine *, compareLines>::iterator &it);
    void checkNext(const std::set<ComparableLine *, compareLines>::iterator &it);

    void insertIntersection(ComparableLine *l1, ComparableLine *l2);
};

#endif // SEGMENTS_H
