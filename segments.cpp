#include "segments.h"
#include "predicates.h"

using namespace std;

Segments::~Segments()
{
    foreach(ComparableLine *line, clines) {
        delete line;
    }
}

void Segments::start()
{
    foreach(ComparableLine *line, clines) {
        delete line;
    }
    clines.clear();
    compared.clear();

    miotla = new Line(-100.0, -100.0, -100.0, 100.0);
    emit drawItem(miotla);

    for(Line *line : lines) {
        if (line->line().p2() < line->line().p1()) {
            line->setLine(QLineF(line->line().p1(), line->line().p2()));
        }
        clines.push_back(new ComparableLine(*line));
        sorted_points.insert(new EndPoint(clines.last(), line->line().p1()));
        sorted_points.insert(new EndPoint(clines.last(), line->line().p2()));
    }
}

bool Segments::nextStep()
{
    if (sorted_points.empty()) {
        return false;
    }
    Point *next = *sorted_points.begin();
    sorted_points.erase(sorted_points.begin());

    miotla->setLine(next->x(), -100.0, next->x(), 100.0);

    EndPoint *end_point = dynamic_cast<EndPoint *>(next);
    if (end_point) {
        if (current_lines.find(end_point->line) != current_lines.end()) {
            linesEnd(end_point);
        } else {
            linesBegin(end_point);
        }
    }

    PointOfIntersection *intersection = dynamic_cast<PointOfIntersection *>(next);
    if (intersection) {
        intesectionPoint(intersection);
    }

    return true;
}

void Segments::linesBegin(EndPoint *point)
{
    point->line->pen().setColor(QColor(255, 0, 0));
    auto position = current_lines.insert(point->line).first;
    checkPrev(position);
    checkNext(position);
}

void Segments::linesEnd(EndPoint *point)
{
    auto position = current_lines.erase(current_lines.find(point->line));
    if (position != current_lines.end()) {
        checkPrev(position);
    }
}

void Segments::intesectionPoint(PointOfIntersection *point)
{
    current_lines.erase(point->line1);
    current_lines.erase(point->line2);
    point->line1->relative_point = point->pos();
    point->line2->relative_point = point->pos();
    auto position = current_lines.insert(point->line1).first;
    checkPrev(position);
    checkNext(position);
    position = current_lines.insert(point->line2).first;
    checkPrev(position);
    checkNext(position);
}

void Segments::checkPrev(const set<ComparableLine *, compareLines>::iterator &it)
{
    if (it == current_lines.begin()) {
        return;
    }

    auto prev = it; prev--;
    insertIntersection(*prev, *it);
}

void Segments::checkNext(const set<ComparableLine *, compareLines>::iterator &it) {
    auto next = it; next++;
    if (next == current_lines.end()) {
        return;
    }
    insertIntersection(*it, *next);
}

void Segments::insertIntersection(ComparableLine *l1, ComparableLine *l2) {
    if (compared.find(make_pair(l1, l2)) != compared.end()) {
        return;
    }
    double t = point_of_intersection(l1->line(), l2->line());
    if (t >= 0.0 && t <= 1.0) {
        QPointF point_pos = l1->line().p1() + (l1->line().p2() - l1->line().p1())*t;
        auto new_point = new PointOfIntersection(point_pos, l1, l2);
        emit drawItem(new_point);
        sorted_points.insert(new_point);
    }
    compared.insert(make_pair(l1, l2));
}


bool compareX::operator()(Point *a, Point *b)
{
    return a->pos() < b->pos();
}


bool compareLines::operator()(ComparableLine *a, ComparableLine *b)
{
    double relp_side = det1(a->line().p1(), a->line().p2(), b->relative_point);
    if (relp_side == 0.0) {
        return det1(a->line().p1(), a->line().p2(), b->line().p2()) < 0.0;
    } else {
        return relp_side < 0.0;
    }
}


bool operator<(const QPointF &a, const QPointF &b)
{
    return a.x() == b.x() ? a.y() < b.y() : a.x() < b.x();
}
